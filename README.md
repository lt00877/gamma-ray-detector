# Gamma Ray Detector

The GRD project aims to design a high performance, low cost gamma ray detector for Cubesat payload.

- [ ] [Visuals]

![PCB V2 Readout](Documents/Img/Screenshot_3.png)

![PCB V2 SiPM](Documents/Img/Screenshot_1.png)

## Features

- [ ] [Features list]

## Hardware

- [ ] [Hardware description]

## Firmware

- [ ] [Firmware dir.]

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.


***


